import React, {Component} from 'react';
import "./styles.css"

class Click extends Component {
    constructor(props) {
        super(props);
        this.state = {
            arr: [" . "," . "," . "]
        }
    }

    render() {

        return (
            <div>
                <div className="arr">{this.state.arr}</div>
                <div className="buttons">
                    <button onClick={() => this.handleClick(1)}>1</button>
                    <button onClick={() => this.handleClick(2)}>2</button>
                    <button onClick={() => this.handleClick(3)}>3</button>
                    <button onClick={() => this.handleClick(4)}>4</button>
                    <button onClick={() => this.handleClick(5)}>5</button>
                </div>
            </div>
        )
    }

    handleClick = (x) => {
        this.state.arr.shift();
        this.state.arr.push(x);
        this.setState({
            arr: this.state.arr
        })
    }
}

export default Click;